package com.gomechanic.CarDetails;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.gomechanic.BaseConfiguration.BaseConfiguration;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class GetBrand extends BaseConfiguration{

	@Test
	public void carBrand() {
		//Request Object
				RequestSpecification httpRequest=RestAssured.given();
				
				//Response Object
				Response response=httpRequest.request(Method.GET, "/api/v2/oauth/vehicles/get_models_by_brand/?brand_id=5");
				//Print Response in Console
				String responseBody=response.getBody().asString();
				System.out.println("Response Body:"+responseBody);
				
				
				
				
	}
}

package com.gomechanic.Vehicle;

import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;

public class GetCarTblId {

	@Test
	public void getCarTblIdTest() {
		Response resp=RestAssured
				.given()
				.auth()
				.oauth2("eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzY29wZXMiOltdLCJleHAiOjE1ODg3ODg2MDIuMCwianRpIjoiYjRiYzNjYWY1ZDFlYTk5ZGM5NmI0MzNjY2M0MzAyNGUwMjNiNDBjNmI0OWYxMTdiYzA5OThmNjFlN2QyNWYzNjE1NWFlOWQyMTYxNmU3OTUiLCJpYXQiOjE1ODYxOTY2MDIuMCwibmJmIjoxNTg2MTk2NjAyLjAsInN1YiI6Ijc1ODY4IiwiYXVkIjoiMyJ9.yWh9XeAq7TZ-8pr4CKbgw9CEL9xwvO53MyEGi2OZoUOGespPP8-zDxvJFP6jcoj18KaF3Dk8C9YwEVgNRtN-10_6CozYN_MbGUaPbbqfolQNzha2SSwLm83XE_Tup_81NFuuD_lPqTmMiQmqDZyhV6wboC7K6qw82vTk_4KxumPgIpd8l8fl9GalxEX3xiMDkC7alynX_zgsdzbDf6dloeGN6u99jez8YYKuJBKiSUGdSWAbM0aogN4du8mKcW2bZfW8yARQXLmSsLbyWkbvwnoWWiGM-CPs05U2D5x2W55PlF52XxLLHGFLO5M_O7-8Y0dUJsVzyhMcQCOAAgkbZrNxHLevUCRa8y-I1r8wJ7Hadp_yd13bBrZXbvA0jwg_pLzGLiZUxkM7rrwiVYFa7ZaIDMiVuNGlZoEaYfNI7GsEtz3J-_hLL3duGz_Eg_HygIMGcppENgewV1R9I95jKAkRlrYczUOI7murIWdLbXKPsgjh5mNXTQi7khcyKZ4JrkgoPz_d8JTbt1ZIbCP6CoWMxvUENVWVddKP6ZcJOMgEbTm8SkjpPYu9y7YYNcvJn2v7wk5e_gBvmjbmGan5jBRAJt3F52etHXRwhNKOzKrr1bP19SXbM9N4fPcvK3SPFSLNtd5umBv0Qed0xKNTNwFdVL5bMrVEkQkzhCmwKD0")
				.get("https://demo.gomechanic.app/api/v1/oauth/vehicles/get_car_tbl_id?brand_id=15&type_id=2&model_id=15");
//		ArrayList<ArrayList<Map<String,Object>>> jsonList = 
//		resp.jsonPath().get("data");
		//System.out.println(jsonList.get(0).size());
		//if(jsonList.size()==17) {
			Assert.assertEquals(resp.getStatusCode(), 200);
			System.out.println("Code"+resp.getStatusCode());
			//System.out.println("Code"+resp.getBody().asString());
			System.out.println(resp.jsonPath().prettify());
			System.out.println("Test Pass Successfully!!");
		//}
}
}

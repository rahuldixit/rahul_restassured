package com.gomechanic.RestAssured;

import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import junit.framework.Assert;

public class RestAssuredAPIDemo1 {

	@Test
	void getResponseDetail() {
		RestAssured.baseURI="https://devleads.gomechanic.app/lead/user/?mobile=8953421721";
		RequestSpecification httpRequest=RestAssured.given();
		Response response=httpRequest.request(Method.GET,"/?mobile=8953421721");
		String responseBody=response.body().asString();
		String otp1=response.jsonPath().get("data.otp");
		System.out.println(otp1);
		int statusCode=response.statusCode();
		Assert.assertEquals(statusCode, 200);
	}
}

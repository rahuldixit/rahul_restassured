package com.gomechanic.Garage;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.gomechanic.BaseConfiguration.BaseConfiguration;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class GETGarageSummary extends BaseConfiguration{

	@Test
	public void garageSummaryTest() {
		
		        //Request Object
				RequestSpecification httpRequest=RestAssured.given();
				
				//Response Object
				Response response=httpRequest.request(Method.GET, "/garages/get_garages_summary/");
				
				//Print Response in Console
				String responseBody=response.getBody().asString();
				System.out.println("Response Body:"+responseBody);
				
				//Print Status
				int statusCode=response.statusCode();
				System.out.println("Status Code:"+statusCode);
				
				//Validate Status Code
				Assert.assertEquals(statusCode, 200);
				String statusLine=response.statusLine();
				
				//Validate Status Line
				System.out.println("Status Line:"+statusLine);
		        Assert.assertEquals(statusLine, "HTTP/1.1 200 OK");

		        //Validate Details of Header from Response
		        //validate content type
		        String contentType=response.header("Content-Type");
		        System.out.println("Content Type:"+contentType);
		        Assert.assertEquals(contentType, "application/json");
		        
		        //validate content length
		        String contentLength=response.header("Content-Length");
		        //Assert.assertEquals(contentLength, "2274");
		        
		        //validate server
		        String serverText=response.header("Server");
		        Assert.assertEquals(serverText, "nginx/1.10.3 (Ubuntu)");

}
}

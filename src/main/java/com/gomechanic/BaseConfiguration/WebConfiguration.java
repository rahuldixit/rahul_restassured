package com.gomechanic.BaseConfiguration;

import java.net.MalformedURLException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

public class WebConfiguration {

	public WebDriver driver;
	@BeforeMethod
	//@Parameters({"browser"})
	public void configureBrowser() throws MalformedURLException {
		
			System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
			driver=new ChromeDriver();
			driver.get("https://dev.web.gomechanic.app/order/20200504885920899");
			driver.manage().window().maximize();
	
		}

	
}
